#!/usr/bin/env python

import roslib; roslib.load_manifest('visualization_marker_tutorials')
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
import rospy
import math

topic = 'surface'
publisher = rospy.Publisher(topic, MarkerArray)
 
rospy.init_node('register')

markerArray = MarkerArray()

count = 0
MARKERS_MAX = 1

while not rospy.is_shutdown():

    marker = Marker()
    marker.header.frame_id = "/base_link"
    marker.type = marker.MESH_RESOURCE
    marker.action = marker.ADD
    marker.scale.x = 14
    marker.scale.y = 14
    marker.scale.z = 14
    marker.color.a = 1.0
    marker.color.r = 1.0
    marker.color.g = 1.0
    marker.color.b = 0.0
    marker.pose.orientation.w = 1
    marker.pose.position.x = -61
    marker.pose.position.y = -695
    marker.pose.position.z = -112
    marker.mesh_resource = "file:///home/tim/ros/quadcopter_test/quadcopter/src/drone_worlds/models/fluss/meshes/wakenitz_bottom.dae"

    if(count > MARKERS_MAX):
        markerArray.markers.pop(0)

    markerArray.markers.append(marker)


    id = 0
    for m in markerArray.markers:
        m.id = id
        id += 1


    publisher.publish(markerArray)

    count += 1

    rospy.sleep(0.01)
