# Hector Quadrotor ROS Pkg

Fork of: https://github.com/tu-darmstadt-ros-pkg/hector_quadrotor

## Modifications include:
* change [teleop node](hector_quadrotor/hector_quadrotor_teleop/src/quadrotor_teleop.cpp) so that pose control resumes from current drone position instead of where teleop left off
* add [urdf files](hector_quadrotor/hector_quadrotor_description/urdf/) for DJI Matrice M100 representation with reference to [DJI M100 description](https://github.com/dji-m100-ros/dji_m100_description)
