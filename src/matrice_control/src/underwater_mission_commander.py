#!/usr/bin/env python
"""
Defines the mission the drone executes underwater
"""
import rospy
from sensor_msgs.msg import Joy, Imu, FluidPressure
from geometry_msgs.msg import Pose, QuaternionStamped, Quaternion, PoseStamped
from uuv_gazebo_ros_plugins_msgs.msg import FloatStamped
from tf.transformations import euler_from_quaternion, quaternion_from_euler
from std_msgs.msg import String
import std_msgs.msg
import numpy as np

underwater_mission_command = [[0,0,0,-2],[-0.7,0.9,0,-2],[0,0,0,-5],[0,0,0,8]]

goal_orrientation = [0,0,0]
mission_index = 0
mission_active = False
active_state = "quadrotor"
prev_state = "quadrotor"
curr_depth = 0
mission_done = True
rotating = False
start_rotate_orr = [0,0,0]


def check_orrientation_reached(current_orrientation):
    global underwater_mission_command, goal_orrientation, mission_index, mission_active, curr_depth, mission_done, rotating, start_rotate_orr
    """
    Checks if the current orrientention is sufficiently close to the goal orrientation. If so, goal index is incremented so that the next goal can be followed
        params: (current_orrientation) the current orrientation of the drone, that is to be compared to the current goal orrientation
    """
    # update goal orrientation
    goal_orrientation = underwater_mission_command[mission_index]

    print("yaw: %s, index: %s, delta: %s, %s, %s, %s, %s" % (current_orrientation[2], mission_index, abs(start_rotate_orr[2] - (current_orrientation[2] + np.pi)) , rotating, underwater_mission_command[mission_index][3], abs(curr_depth-underwater_mission_command[mission_index][3]), curr_depth))

    if rotating:
        if abs(start_rotate_orr[2] - (current_orrientation[2] + np.pi)) < np.pi/20:
            rotating = False
            if not(mission_index == len(underwater_mission_command)-1):
                # if we have not reached the end of our mission, increment mission index
                mission_index = mission_index + 1

    if abs(curr_depth-underwater_mission_command[mission_index][3]) < 0.5  and not(rotating):
        # check if we have reached our goal orrientation
        if not(mission_index == len(underwater_mission_command)-1):
            # if we have not reached the end of our mission, increment mission index
            mission_index = mission_index + 1

            if underwater_mission_command[mission_index][0] != 0:
                rotating = True
                start_rotate_orr = current_orrientation
                start_rotate_orr[2] = start_rotate_orr[2] - np.pi/10 + np.pi

        else:
            # end of mission, reset for next mission
            #mission_index = 0
            mission_done = True

    

def depth_callback(msg):
    global curr_depth, active_state
    """
    Calculate current depth based on pressure Data
    """

    pressure = msg.fluid_pressure

    if active_state == "quadrotor":
        sign = -1
    else:
        sign = 1

    curr_depth = (-0.087719 * pressure + 8.77193) * sign


def imu_callback(msg):
    global orrientation_pub, underwater_mission_command, mission_index, desired_depth, mission_active
    """
    Whenever drone changes position, new desired position is calculated.
    Publishes set pose for underwater PID Controller as PoseStamped with:
        orrientation: the desired orrientation of the drone
        position.z: desired depth of the drone

    params: (msg) the callback from the drones IMU sensor
    """
    if not(mission_active):
        return None

    # convert IMU data to euler angles
    (curr_roll, curr_pitch, curr_yaw) = euler_from_quaternion((msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w))

    # new set-point to be followed by the PID controller
    desired_orrientation = quaternion_from_euler(underwater_mission_command[mission_index][0], underwater_mission_command[mission_index][1], underwater_mission_command[mission_index][2])

    check_orrientation_reached([curr_roll, curr_pitch, curr_yaw])

    return_pose = PoseStamped()
    return_pose.header.frame_id = "world"
    return_pose.header.stamp = rospy.Time.now()

    return_orrientation = Quaternion()
    return_orrientation.x = desired_orrientation[0]
    return_orrientation.y = desired_orrientation[1]
    return_orrientation.z = desired_orrientation[2]
    return_orrientation.w = desired_orrientation[3]

    #print("return_orientation x: %s, y: %s, z: %s, w: %s" % (return_orrientation.x,return_orrientation.y,return_orrientation.z,return_orrientation.w))

    return_pose.pose.orientation = return_orrientation
    return_pose.pose.position.z = underwater_mission_command[mission_index][3]

    #print("missionIndex: %s, dRoll: %s, dPitch: %s, dYaw: %s" % (mission_index,float(curr_roll-underwater_mission_command[mission_index][0]), float(curr_pitch-underwater_mission_command[mission_index][1]), float(curr_yaw-underwater_mission_command[mission_index][2])))

    orrientation_pub.publish(return_pose)

def active_state_callback(state):
    global active_state, prev_state, mission_active, mission_done, mission_index, rotating
    """
    Updates the drone state and starts the mission if change is detected
    """
    prev_state = active_state
    active_state = state.data

    if active_state == "matrice" and prev_state == "quadrotor":
        print("starting underwater mission")
        mission_active = True
        mission_done = False
        mission_index = 0
        rotating = False
    elif active_state == "quadrotor" and prev_state == "matrice":
        print("underwater mission complete")
        mission_active = False
        mission_done = True

def underwater_mission_palanner():
    global orrientation_pub, underwater_mission_command
    rospy.init_node("underwater_mission_commander")
  
    orrientation_pub = rospy.Publisher('/matrice/pose_commander', PoseStamped, queue_size=10)

    orrientation_sub = rospy.Subscriber('/matrice/imu', Imu, imu_callback)
    depth_sub = rospy.Subscriber('/matrice/pressure', FluidPressure, depth_callback)
    rospy.Subscriber("/drone/active_state", String, active_state_callback)

    rospy.spin()

if __name__ == '__main__':
    underwater_mission_palanner()