#!/usr/bin/env python

"""
Main PID Controller for Drone during autonomous operation.
"""

import rospy
from sensor_msgs.msg import Joy, Imu, FluidPressure, LaserScan
from geometry_msgs.msg import Pose, QuaternionStamped, Quaternion, PoseStamped
from uuv_gazebo_ros_plugins_msgs.msg import FloatStamped
from tf.transformations import euler_from_quaternion
import std_msgs.msg
import numpy as np
from std_msgs.msg import String

##external controller mode
kill_switch = 0
manual_control = 0

## Set Points:
set_point_roll = 0
set_point_pitch = 0
set_point_yaw = 0

set_point_depth = 0

## PID:
error_roll = 0
error_pitch = 0
error_yaw = 0

prev_error_roll = 0
prev_error_pitch = 0
prev_error_yaw = 0

curr_roll = 0
curr_pitch = 0
curr_yaw = 0
curr_depth = 0

delta_time = 0
last_time = 0

iMem_roll = 0
iMem_pitch = 0
iMem_yaw = 0

output_depth = 0

## joy variables 
left_rot = 0
right_rot = 0
add_thrus = 0
landing_action = 0

drone_rep_state = "matrice"

## PID Parameters -------------------------
kp_roll = 3          # 3
ki_roll = 0.005         # 0.005
kd_roll = 12            # 12
kp_pitch = kp_roll 
ki_pitch = ki_roll
kd_pitch = kd_roll
kp_yaw = 0
ki_yaw = 0
kd_yaw = 0
## ----------------------------------------

# Publishers for engine thrust
thruster_0_pub = None 
thruster_1_pub = None  
thruster_2_pub = None  
thruster_3_pub = None 


def calc_PID():
    global add_thrus, left_rot, right_rot, set_point_pitch, set_point_roll, set_point_yaw, set_point_depth, error_roll, error_pitch, error_yaw, prev_error_roll, prev_error_pitch, prev_error_yaw, curr_depth, curr_roll, curr_pitch, curr_yaw, delta_time, last_time, kp_roll, ki_roll, kd_roll, kp_pitch, ki_pitch, kd_pitch, kp_yaw, ki_yaw, kd_yaw, pMem_roll, pMem_yaw, pMem_pitch, iMem_roll, iMem_pitch, iMem_yaw, dMem_roll, dMem_pitch, dMem_yaw, thruster_0_pub, thruster_1_pub, thruster_2_pub, thruster_3_pub, output_depth, kill_switch, manual_control
    
    """
    Calculate the thrust output for each Rotor based on error data
    """

    local_depth = output_depth

    # calculate time delata
    curr_time = rospy.Time.now().to_sec()
    delta_time = curr_time - last_time
    last_time = curr_time

    # calculate controller error terms
    error_roll = float(curr_roll)  - set_point_roll
    error_pitch = float(curr_pitch)  - set_point_pitch
    error_yaw = float(curr_yaw)  - set_point_yaw


    # -----------------------------------------------------------------------
    # Main PID calculations
    # KP
    pMem_roll = kp_roll * error_roll 
    pMem_pitch = kp_pitch * error_pitch 
    pMem_yaw = kp_yaw * error_yaw

    # KI
    iMem_roll += error_roll * delta_time
    iMem_pitch += error_pitch * delta_time
    iMem_yaw += error_yaw * delta_time

    if(iMem_roll > 400): iMem_roll = 400
    if(iMem_roll < -400): iMem_roll = -400
    if(iMem_pitch > 400): iMem_pitch = 400
    if(iMem_pitch < -400): iMem_pitch = -400
    if(iMem_yaw > 400): iMem_yaw = 400
    if(iMem_yaw < -400): iMem_yaw = 400

    # KD
    dMem_roll = (error_roll - prev_error_roll) / delta_time
    dMem_pitch = (error_pitch - prev_error_pitch) / delta_time
    dMem_yaw = (error_yaw - prev_error_yaw) / delta_time
    # -----------------------------------------------------------------------



    # save current error as last error for next iteration
    prev_error_roll = error_roll
    prev_error_pitch = error_pitch
    prev_error_yaw = error_yaw

    # calculate PID output
    output_roll = pMem_roll + ki_roll * iMem_roll + kd_roll * dMem_roll
    output_pitch = pMem_pitch + ki_pitch * iMem_pitch + kd_pitch * dMem_pitch
    output_yaw = 0#pMem_yaw + ki_yaw * iMem_yaw + kd_yaw * dMem_yaw

    if (manual_control == 1) :
        #output_roll = set_point_roll * 1.5
        #output_pitch = set_point_pitch * 1.5
        local_depth = (set_point_depth + 1) / 3
        


    # br: Back Right
	# bl: Back Left
	# fl: Front Left
	# fr: Front Right
    # calculate thrust in gazebo frame of reference


    br_thrust = output_roll - output_pitch - output_yaw + local_depth
    bl_thrust = -output_roll - output_pitch + output_yaw + local_depth
    fl_thrust = -output_roll + output_pitch - output_yaw + local_depth
    fr_thrust = output_roll + output_pitch + output_yaw + local_depth

    #if abs(br_thrust) > 10: br_thrust = np.sign(br_thrust) * 10
    #if abs(bl_thrust) > 10: bl_thrust = np.sign(bl_thrust) * 10
    #if abs(fl_thrust) > 10: fl_thrust = np.sign(fl_thrust) * 10
    #if abs(fr_thrust) > 10: fr_thrust = np.sign(fr_thrust) * 10

    #print("r: %s, p: %s, y: %s" % (output_roll, output_pitch, output_yaw))
    #print("br: %s, bl: %s, fl: %s, fr: %s" % (br_thrust, bl_thrust, fl_thrust, fr_thrust))
    #print("sr: %s, ir: %s, sp: %s, ip: %s" % (set_point_roll, curr_roll, set_point_pitch, curr_pitch))

    plotData = LaserScan()
    #plotData.layout.dim.label = "data"
    #plotData.layout.dim.size = 4
    #plotData.layout.dim.stride = 4
    #plotData.layout.data_offset = 0
    plotData.header.stamp = rospy.Time.now()
    plotData.ranges.append(set_point_roll)
    plotData.ranges.append(curr_roll)
    plotData.ranges.append(set_point_pitch)
    plotData.ranges.append(curr_pitch)

    # debug Plotting topic 
    plotP = rospy.Publisher("/plot",LaserScan,queue_size = 10)

    plotP.publish(plotData)
    
    thrust = [FloatStamped(), FloatStamped(), FloatStamped(), FloatStamped()]

    for i in range(len(thrust)):
        # add Timestamp to messages
        thrust[i].header.stamp = rospy.Time.now()

    # add Thrust data to message for each thruster
    thrust[0].data = fl_thrust * (1 - kill_switch) + (left_rot * 1.75) - (right_rot * 1.75) + add_thrus
    thrust[1].data = bl_thrust * (1 - kill_switch) + (left_rot * 1.75) - (right_rot * 1.75) + add_thrus
    thrust[2].data = fr_thrust * (1 - kill_switch) - (left_rot * 1.75) + (right_rot * 1.75) + add_thrus
    thrust[3].data = br_thrust * (1 - kill_switch) - (left_rot * 1.75) + (right_rot * 1.75) + add_thrus

    # publish Thruster messages
    thruster_0_pub.publish(thrust[0])
    thruster_1_pub.publish(thrust[1])
    thruster_2_pub.publish(thrust[2])
    thruster_3_pub.publish(thrust[3])



    


def imu_callback(msg):
    global curr_roll, curr_pitch, curr_yaw
    """
    New data from internal Drone IMU used to calculating roll-, pitch- and yaw-error
    """
    (roll, pitch, yaw) = euler_from_quaternion((msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w))
    curr_roll = roll
    curr_pitch = pitch
    curr_yaw = yaw

    calc_PID()

    #print("roll: %s, pitch: %s, yaw: %s" % (roll, pitch, yaw))

def depth_callback(msg):
    global curr_depth, output_depth, curr_roll, curr_pitch, set_point_depth, set_point_pitch, set_point_roll, drone_rep_state, landing_action
    """
    Calculate current depth of drone based on barometric pressure data
    """
    pressure = msg.fluid_pressure

    if drone_rep_state == "quadrotor":
        sign = -1
        offset = 1
    else:
        sign = 1
        offset = 1

    # calc depth from pressure
    curr_depth = (-0.087719 * pressure + 8.77193) * sign
    print(curr_depth)

    error_depth = curr_depth - set_point_depth

    kp_depth = 1.8

    depth_pid_output = error_depth * kp_depth * offset - 2

    output_depth = -depth_pid_output

    if landing_action == 1 and not(drone_rep_state == "matrice"):
        output_depth = 8


def set_point_callback(msg):
    global set_point_roll, set_point_pitch, set_point_yaw, set_point_depth, kill_switch, manual_control
    """
    Desired orrientation and depth of vehicle changed
    """
    (roll, pitch, yaw) = euler_from_quaternion((msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z, msg.pose.orientation.w))
    set_point_pitch = pitch 
    set_point_roll = roll 
    set_point_yaw = yaw * 0

    set_point_depth = msg.pose.position.z

    #deactivates all motors completely
    kill_switch = msg.pose.position.x

    #disables controller and enables full manual control
    manual_control = msg.pose.position.y

def joy_callback(msg):
    global left_rot, right_rot, add_thrus, landing_action
    left_rot = msg.buttons[4]
    right_rot = msg.buttons[5]

    landing_action = msg.buttons[2]

    add_thrus = (-2*(2-(msg.axes[5]+1))-1) + ((2*(2-(msg.axes[2]+1))-1)+1)

def state_callback(state):
    global drone_rep_state
    drone_rep_state = state.data

def auto_control():
    global thruster_0_pub, thruster_1_pub, thruster_2_pub, thruster_3_pub
    rospy.init_node("Matrice_autocontrol")
  
    thruster_0_pub = rospy.Publisher('/matrice/thrusters/0/input', FloatStamped, queue_size=10)
    thruster_1_pub = rospy.Publisher('/matrice/thrusters/1/input', FloatStamped, queue_size=10)
    thruster_2_pub = rospy.Publisher('/matrice/thrusters/2/input', FloatStamped, queue_size=10)
    thruster_3_pub = rospy.Publisher('/matrice/thrusters/3/input', FloatStamped, queue_size=10)

    set_pint_sub = rospy.Subscriber('/matrice/pose_commander', PoseStamped, set_point_callback)

    orrientation_sub = rospy.Subscriber('/matrice/imu', Imu, imu_callback)
    depth_sub = rospy.Subscriber('/matrice/pressure', FluidPressure, depth_callback)
    state_sub = rospy.Subscriber("/drone/active_state", String, state_callback)
    ControllSub = rospy.Subscriber('/joy', Joy, joy_callback)

    rospy.spin()

if __name__ == '__main__':
    auto_control()

