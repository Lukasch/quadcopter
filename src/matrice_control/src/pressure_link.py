#!/usr/bin/env python
"""
Detect entering of water by IMU Data
"""
import rospy
from sensor_msgs.msg import Joy, Imu, FluidPressure
from geometry_msgs.msg import Pose, QuaternionStamped, Quaternion, PoseStamped
from uuv_gazebo_ros_plugins_msgs.msg import FloatStamped
from tf.transformations import euler_from_quaternion, quaternion_from_euler
import std_msgs.msg
import numpy as np
from std_msgs.msg import String

prev_acc = 0
acc = 0
toggle = False
state_pub = None
orrientation_pub = None

def IMU_callback(imu_data):
    global prev_acc, acc, toggle

    prev_acc = acc
    acc = imu_data.linear_acceleration.z

    delta = acc - prev_acc

    if delta <= -5:
        toggle = False
        print("Air")
        state_pub.publish("quadrotor")

    elif delta >= 4:
        toggle = True
        print("Water")
        state_pub.publish("matrice")

    #print(acc)


def pressure_link():
    global state_pub, orrientation_pub
    rospy.init_node("active_state")

    state_pub = rospy.Publisher("/drone/active_state", String, queue_size=10)
    orrientation_pub = rospy.Publisher('/matrice/pose_commander', PoseStamped, queue_size=10)

    ControllSub = rospy.Subscriber('/matrice/imu', Imu, IMU_callback)

    rospy.spin()

if __name__ == '__main__':
    pressure_link()
