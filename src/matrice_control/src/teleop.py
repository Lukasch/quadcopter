#!/usr/bin/env python
import rospy
from sensor_msgs.msg import Joy
from geometry_msgs.msg import Pose, Wrench, WrenchStamped
from uuv_gazebo_ros_plugins_msgs.msg import FloatStamped
import std_msgs.msg

def joy_callback(msg):
    thrust = [FloatStamped(), FloatStamped(), FloatStamped(), FloatStamped()]

    for i in range(len(thrust)):
        thrust[i].header.stamp = rospy.Time.now()
        thrust[i].data = 0

    if msg.buttons[4] == 1:
        for i in range(len(thrust)):
            thrust[i].data = msg.axes[1] * 10

    #print("Thrust: %s, %s, %s, %s" % (thrust[0].data, thrust[1].data, thrust[2].data, thrust[3].data))

    thrust[3].data = thrust[3].data 
    thrust[0].data = thrust[0].data 

    thruster_0_pub.publish(thrust[0])
    thruster_1_pub.publish(thrust[1])
    thruster_2_pub.publish(thrust[2])
    thruster_3_pub.publish(thrust[3])

    thrust[3].data = thrust[3].data

    wrench = Wrench()
    wrench.force.x = thrust[3].data
    wrench.force.y = thrust[3].data
    wrench.force.z = thrust[3].data
    wrench.torque.x = thrust[3].data
    wrench.torque.y = thrust[3].data
    wrench.torque.z = thrust[3].data

    #wrenchPub.publish(wrench)


rospy.init_node("Matrice_UUV_Teleop")
  
thruster_0_pub = rospy.Publisher('/matrice/thrusters/0/input', FloatStamped, queue_size=10)
thruster_1_pub = rospy.Publisher('/matrice/thrusters/1/input', FloatStamped, queue_size=10)
thruster_2_pub = rospy.Publisher('/matrice/thrusters/2/input', FloatStamped, queue_size=10)
thruster_3_pub = rospy.Publisher('/matrice/thrusters/3/input', FloatStamped, queue_size=10)

wrenchPub = rospy.Publisher('/matrice/thrusters/3/thrust_wrench', Wrench, queue_size=10)

ControllSub = rospy.Subscriber('/joy', Joy, joy_callback)

rospy.spin()