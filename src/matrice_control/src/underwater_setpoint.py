#!/usr/bin/env python
"""
Define desired orrientation and depth of Vehicle for use with manual control
"""
import rospy
from sensor_msgs.msg import Joy, Imu, FluidPressure
from geometry_msgs.msg import Pose, QuaternionStamped, Quaternion, PoseStamped
from uuv_gazebo_ros_plugins_msgs.msg import FloatStamped
from tf.transformations import euler_from_quaternion, quaternion_from_euler
import std_msgs.msg
import numpy as np


def joy_callback(msg):

    #print( msg.axes[8] )

    #print(msg.axes[0])
    if (msg.axes[6] == 1 or msg.buttons[6] == 1) :
        desired_orrientation = quaternion_from_euler(msg.axes[3] * -1 * 3.1415, msg.axes[4] *  3.1415, msg.axes[0] * -1 * 3.1415)
    else :
        desired_orrientation = quaternion_from_euler(0, 0, 0)

    return_val = Quaternion()
    return_val.x = desired_orrientation[0]
    return_val.y = desired_orrientation[1]
    return_val.z = desired_orrientation[2]
    return_val.w = desired_orrientation[3]

    print("0: %s, 1: %s, 2: %s" % (msg.axes[3] * -1 * 3.1415, msg.axes[4] *  3.1415, msg.axes[0] * -1 * 3.1415))


    retPose = PoseStamped()
    #retPose.pose.position.z = (-5*(2-(msg.axes[5]+1))-1) + ((5*(2-(msg.axes[2]+1))-1)+1)
    retPose.pose.position.z = (msg.axes[1] * 11) - 1
    #start button
    retPose.pose.position.x = msg.buttons[7]
    #steuerkreuz nach oben
    retPose.pose.position.y = msg.buttons[6]
    retPose.header.frame_id = "world"
    retPose.header.stamp = rospy.Time.now()
    retPose.pose.orientation = return_val

    orrientation_pub.publish(retPose)



def underwater_setpoint():
    global orrientation_pub
    rospy.init_node("setpoint_node")
  
    orrientation_pub = rospy.Publisher('/matrice/pose_commander', PoseStamped, queue_size=10)

    ControllSub = rospy.Subscriber('/joy', Joy, joy_callback)

    rospy.spin()

if __name__ == '__main__':
    underwater_setpoint()
