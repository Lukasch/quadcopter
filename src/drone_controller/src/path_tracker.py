#!/usr/bin/env python

"""
Subscribes to the drone position and periodically appends it to the Path for display in rviz
"""
import rospy
from nav_msgs.msg import Path
from geometry_msgs.msg import PoseStamped, Pose
import numpy as np

pub = rospy.Publisher('/drone/path', Path, queue_size=10)
path = Path()
time = None
lastPose = PoseStamped()

def pose_dist(pose1, pose2):
    """
    Calculate distance between two poses
    """
    currPoint = pose1.pose.position
    goalPoint = pose2.pose.position

    dist = np.sqrt((currPoint.x - goalPoint.x)**2 + (currPoint.y - goalPoint.y)**2 + (currPoint.z - goalPoint.z)**2) 

    return dist

def pose_callback(data):
    global path, pub, time, lastPose
    """
    Add new pose to path every 500ms if drone has moved during this time
    """
    
    tmpTime = rospy.Time.now().to_sec()

    # check if 200ms passed and if drone has moved
    if tmpTime - time > 0.5 and not(pose_dist(data, lastPose) < 0.0001) and data.pose.position.x < 8000:
        time = tmpTime

        path.poses.append(data)
        path.header.stamp = rospy.Time.now()
        path.header.frame_id = "world"
        pub.publish(path)
    
    lastPose = data
    
def path_tracker():
    global time

    rospy.init_node('drone_path', anonymous=True)

    rospy.Subscriber("/drone/pose", PoseStamped, pose_callback)

    time = rospy.Time.now().to_sec()

    rospy.spin()

if __name__ == '__main__':
    path_tracker()