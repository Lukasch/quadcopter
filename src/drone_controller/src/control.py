#!/usr/bin/env python
#---------------------------------------------------
from pid import PID
import rospy
from gazebo_msgs.msg import ModelStates
from std_msgs.msg import Float64MultiArray, Float32
from geometry_msgs.msg import Pose, Twist, WrenchStamped, PoseStamped, Point
from tf.transformations import euler_from_quaternion
from sensor_msgs.msg import Joy
from hector_uav_msgs.msg import ThrustCommand

#hector_uav_msgs/ThrustCommand
def talker():
	pub = rospy.Publisher('/command/pose', PoseStamped, queue_size=10)
	rospy.init_node('Control', anonymous=True)
	rate = rospy.Rate(10) # 10hz
	while not rospy.is_shutdown():
		msg = PoseStamped()
		msg.header.frame_id = "world"
		msg.pose.position.x = 0
		msg.pose.position.y = 0
		msg.pose.position.z = 5
		rospy.loginfo(msg)
		pub.publish(msg)

if __name__ == '__main__':
	try:
		talker()
	except rospy.ROSInterruptException:
		pass

