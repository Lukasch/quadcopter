#!/usr/bin/env python

"""
Switches between underwater and air model of drone depending on position of drone (submerged or not)
"""

import rospy
import rospkg
import roslaunch
import roslib
import os
import subprocess
from geometry_msgs.msg import PoseStamped, Pose
from sensor_msgs.msg import Joy
from gazebo_msgs.srv import SetModelState
from gazebo_msgs.msg import ModelStates, ModelState
from std_msgs.msg import String

switchButtonVal = 0
currentDronePosition = Pose()
lastDronePosition = Pose()
fuckingFarAway = Pose()

currentModel = "quadrotor"
tmpModel = "quadrotor"

switching = False

lastSwitchTime = 0
currModelPub = None


def remove_model(model):
    global fuckingFarAway
    """
    "Removes" model model by teleporting it far away
        param: (model) the model to be removed
    """

    # define new modelstate with position set to far away
    state_msg = ModelState()
    state_msg.model_name = model
    state_msg.pose = fuckingFarAway

    # wait for model state service
    rospy.wait_for_service('/gazebo/set_model_state')
    try:
        # set model state of model to update position
        set_state = rospy.ServiceProxy('/gazebo/set_model_state', SetModelState)
        resp = set_state( state_msg )

    except rospy.ServiceException, e:
        print "Service call failed: %s" % e


def spawn_model(model):
    global lastDronePosition
    """
    Moves model model to last position of drone
        param: (model) the model to be moved to lastDronePosition
    """

    # new model state with updated position
    state_msg = ModelState()
    state_msg.model_name = model
    state_msg.pose = lastDronePosition

    # wait for set model state service
    rospy.wait_for_service('/gazebo/set_model_state')
    try:
        set_state = rospy.ServiceProxy('/gazebo/set_model_state', SetModelState)
        resp = set_state( state_msg )

    except rospy.ServiceException, e:
        print "Service call failed: %s" % e

def joy_callback(msg):
    global switchButtonVal, removeButtonVal, currentDronePosition, currentModel, switching
    """
    Models are also switched when hitting the A button on the controller (for testing only)
        params: (msg) the joy message sent by the controller
    """

    switching = True

    if msg.buttons[0] == 1 and not(msg.buttons[0] == switchButtonVal):
        # save position of active model
        currentPosition = currentDronePosition
        # remove current drone model
        remove_model(currentModel)
        currentDronePosition = currentPosition
        # spawn the other presentaion of the drone
        spawn_model(invert(currentModel))
        # save current state
        currentModel = invert(currentModel)
        
    switching = False
	
    switchButtonVal = msg.buttons[0]


def invert(model):
    """
    Inverts string representation of the model
        params: (model) the model string to be inverted
    """
    ret = "matrice"
    if str(model) == ret:
        ret = "quadrotor"

    return ret


def auto_switch():
    global currentDronePosition, lastDronePosition, currentModel, switching, tmpModel, lastSwitchTime
    """
    Switches between the drone representations when the water surface is crossed
    """

    switching = True

    # take derivatice of drone z Position
    dHeight = currentDronePosition.position.z - lastDronePosition.position.z

    #print("dHeight: %s, height: %s, x: %s" % (dHeight, currentDronePosition.position.z, currentDronePosition.position.x < 8000))

    if dHeight < 0 and currentDronePosition.position.z < 0 and currentDronePosition.position.x < 8000:
        # we are falling and current position is under water surface
        if tmpModel == "quadrotor" and rospy.Time.now().to_sec() - lastSwitchTime > 1:
            lastSwitchTime = rospy.Time.now().to_sec()
            print("auto-switching to matrice")
            tmpModel = "matrice"
            remove_model("quadrotor")
            spawn_model("matrice")
            currentModel = "matrice"

    elif dHeight > 0 and currentDronePosition.position.z > 2 and currentDronePosition.position.x < 8000:
        # we are rising and are over the water surface
        if tmpModel == "matrice" and rospy.Time.now().to_sec() - lastSwitchTime > 1:
            lastSwitchTime = rospy.Time.now().to_sec()
            print("auto-switching to quadrotor")
            tmpModel = "quadrotor"
            remove_model("matrice")
            spawn_model("quadrotor")
            currentModel = "quadrotor"

    switching = False

def model_state_callback(model_state):
    global currentModel, currentDronePosition, lastDronePosition, switching
    """
    Callback for gazebo model state
        params: (model_states) the model states received from gazebo
    """
    if not(switching):
        # only update params when not currently switching between representations

        # get index of current model
        index = model_state.name.index(currentModel)

        # get pose of current model
        tmpPose = model_state.pose[index]
        if tmpPose.position.x < 8000:
            # only set new pose if within bounds
            lastDronePosition = currentDronePosition
            currentDronePosition = tmpPose
            # auto_switch checks if representation needs to be updated
            auto_switch()

    # publish the active drone model for use in other nodes
    currModelPub.publish(currentModel)


def model_switcher():
    global fuckingFarAway, currModelPub
    """
    Script for dynamically switching between model representations
    """
    rospy.init_node('model_switcher', anonymous=True)

    ControllSub = rospy.Subscriber('/joy', Joy, joy_callback)

    rospy.Subscriber("/gazebo/model_states", ModelStates, model_state_callback)

    currModelPub = rospy.Publisher("/drone/active_state", String, queue_size=10)

    # define position the unwanted model is moved to when not in use
    fuckingFarAway.position.x = 10000
    fuckingFarAway.position.y = 10000
    fuckingFarAway.position.z = 1

    rospy.spin()

if __name__ == '__main__':
    model_switcher()