#!/usr/bin/env python

"""
Takes five 2D Waypoints from RVIZ and publishes them to   as a PoseArray for the drone to follow.
"""

import rospy
from geometry_msgs.msg import PoseStamped, PoseArray, Pose

pub = rospy.Publisher('/mission/waypoints', PoseArray, queue_size=10)

waypoints = PoseArray()
waypointCounter = 0

def rviz_control_callback(data):
    """
    Takes 2D Nav-Goals from RVIZ and adds them to a list of waypoints.
    After 5 waypoints have been submitted, they are passed to the mission_planner as a PoseArray in order to be followed.
    """

    global waypointCounter,waypoints,pub

    # set height to fixed height of 5
    data.pose.position.z = 5
    rospy.loginfo("Ok. Received %s. Adding to flight-plan. %s more Points needed. \n\n" % (data.pose, 4-waypointCounter))

    # PoseArray takes poses, not PoseStamped
    tmpPose = Pose()

    # Take data from data and add to empty Pose obj
    tmpPose.position = data.pose.position
    tmpPose.orientation = data.pose.orientation

    # Add Pose to list of waypoints
    waypoints.poses.append(tmpPose)

    waypointCounter += 1

    if waypointCounter == 5:
        # after 5 waypoints habe been submitted, start the mission
        print("Starting Mission. Got %s Waypoints." % len(waypoints.poses))
        waypoints.header.stamp = rospy.Time.now()
        pub.publish(waypoints)

        # clear waypoints for next mission
        waypoints = PoseArray()
        waypointCounter = 0
    
def rviz_control_relay():

    rospy.init_node('rviz_control_relay', anonymous=True)

    rospy.Subscriber("move_base_simple/goal", PoseStamped, rviz_control_callback)

    rospy.spin()

if __name__ == '__main__':
    rviz_control_relay()