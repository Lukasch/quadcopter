#!/usr/bin/env python

"""
Takes 2D Waypoints from RVIZ and publishes them to the /command/pose topic used by the drone during flight.
"""

import rospy
from geometry_msgs.msg import PoseStamped

pub = rospy.Publisher('/command/pose', PoseStamped, queue_size=10)

def rviz_control_callback(data):
    data.pose.position.z = 5
    rospy.loginfo("Ok. Flying to %s", data.pose)
    pub.publish(data)
    
def rviz_control_relay():

    rospy.init_node('rviz_control_relay', anonymous=True)

    rospy.Subscriber("move_base_simple/goal", PoseStamped, rviz_control_callback)

    rospy.spin()

if __name__ == '__main__':
    rviz_control_relay()