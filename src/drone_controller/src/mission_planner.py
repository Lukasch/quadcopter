#!/usr/bin/env python

"""
Takes an array of poses from user input and relays them, one after the other, to the drone.
Publishes a new Waypoint when the last one has been reached.
"""

import rospy
from geometry_msgs.msg import PoseStamped, PoseArray, Pose
import numpy as np
from nav_msgs.msg import Path
from hector_uav_msgs.srv import EnableMotors

pub = rospy.Publisher('/command/pose', PoseStamped, queue_size=10)

currentGoal = Pose()
currentPose = Pose()
mission = PoseArray()

currentGoalIndex = 0

missionDataReceived = False

pathPub = None


def publish_new_goal(goal):
    global pub
    """
    Publishes the next Pose the drone should be in.
    """
    
    # empty PoseStamped obj 
    g = PoseStamped()
    g.header.stamp = rospy.Time.now()
    g.header.frame_id = "world"

    # set pose to pose of next goal waypoint
    g.pose = goal

    #print("New Waypoint Reached. Next Goal: %s " % g)

    pub.publish(g)


def check_goal_reached(current, goal):
    """
    Check if current Pose is sufficiently close to goal Pose.
    If closer than 0.1m, the point has been reached.

    returns:    True if current Pose within 0.1m of goal Pose
                False otherwise
    """

    currPoint = current.position
    goalPoint = goal.position

    # calculate distance between the two points
    dist2goal = np.sqrt((currPoint.x - goalPoint.x)**2 + (currPoint.y - goalPoint.y)**2 + (currPoint.z - goalPoint.z)**2) 

    # if goal is underwater, goal is reached when copter is low 
    if goalPoint.z < 0:
        return currPoint.z < 0

    if dist2goal < 0.1:
        return True
    else:
        #print("Distance to next waypoint: %s m" % dist2goal)
        return False


def drone_position_callback(data):
    global missionDataReceived, currentGoal, mission, currentGoalIndex, currentPose
    """
    Drone changed position. Check if waypoint has been reached yet.
    """

    # extract current pose of drone
    currentPose = data.pose

    if missionDataReceived:
    # if we have an active mission (i.e. waypoints to follow)
        if check_goal_reached(currentPose, currentGoal):
        # check if we have reached the next point yet

            # if so, increment counter and set next waypoint in list as active goal
            currentGoalIndex += 1
            print("Goal Reched. Flying to %s" % currentGoalIndex)
            
            if currentGoalIndex == len(mission.poses)-1:
            # check if we have reached all of our waypoints, if so end mission
                missionDataReceived = False
                #print("Flying to last Waypoint. Mission Complete. Waiting for new Mission Data...")

            currentGoal = mission.poses[currentGoalIndex]
            publish_new_goal(currentGoal)

    if len(mission.poses) > 1 and check_goal_reached(currentPose, mission.poses[len(mission.poses)-1]) and currentGoalIndex == len(mission.poses)-1:
        # reached last waypoint (returned home) -> shut off motors
        start_motors = rospy.ServiceProxy('enable_motors', EnableMotors)
        resp = start_motors(False)

def mission_callback(data):
    global missionDataReceived, currentGoal, mission, currentGoalIndex, currentPose, pathPub
    """
    User submitted new mission.
    input: 
        data(PoseArray) : The mission waypoints
    """

    rospy.loginfo("Mission Data received. Updating Flight Plan.")
    missionDataReceived = True
    mission = data

    print("got mission")

    # add waypoints for underwater mission
    missionLength = len(mission.poses)
    for i in range(1,missionLength*3,3):
        currX = mission.poses[i-1].position.x
        currY = mission.poses[i-1].position.y

        tt = Pose()
        tt.position.x = currX
        tt.position.y = currY
        tt.position.z = -1
        mission.poses.insert(i, tt)

        ttt = Pose()
        ttt.position.x = currX
        ttt.position.y = currY
        ttt.position.z = 5
        mission.poses.insert(i+1, ttt)



    # add waypoint directly above drone as first waypoint
    tmpPose = currentPose
    tmpPose.position.z = 5
    mission.poses.insert(0, tmpPose)
    t2 = Pose()
    t2.position.x = tmpPose.position.x
    t2.position.y = tmpPose.position.y
    t2.position.z = 1
    mission.poses.insert(len(mission.poses),t2)

    # return home after mission
    t3 = Pose()
    t3.position.x = tmpPose.position.x
    t3.position.y = tmpPose.position.y
    t3.position.z = 5
    mission.poses.insert(len(mission.poses)-1,t3)

    # display mission path in rviz
    missionPath = Path()
    missionPath.header.stamp = rospy.Time.now()
    missionPath.header.frame_id = "world"
    for i in range(len(mission.poses)):
        tmpPose = PoseStamped()
        tmpPose.header.stamp = rospy.Time.now()
        tmpPose.header.frame_id = "world"
        tmpPose.pose = mission.poses[i]
        missionPath.poses.append(tmpPose)

    pathPub.publish(missionPath)

    print("new mission")
    for i in range(len(mission.poses)):
        print("%s: %s, %s, %s" % (i, mission.poses[i].position.x, mission.poses[i].position.y, mission.poses[i].position.z))

    # Start the drone motors
    start_motors = rospy.ServiceProxy('enable_motors', EnableMotors)
    resp = start_motors(True)

    

    currentGoalIndex = 0
    currentGoal = mission.poses[0]
    publish_new_goal(currentGoal)
    
def mission_planner():
    global pathPub
    rospy.init_node('mission_planner', anonymous=True)

    rospy.Subscriber("/mission/waypoints", PoseArray, mission_callback)

    rospy.Subscriber("/ground_truth_to_tf/pose", PoseStamped, drone_position_callback)

    pathPub = rospy.Publisher('/mission/path', Path, queue_size=10)

    rospy.spin()

if __name__ == '__main__':
    mission_planner()

