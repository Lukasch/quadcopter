# Bachelorproject Hybrid-Aerial Drone

To launch the full Scenario:
`roslaunch drone_controller start_wakenitz.launch`

Drone can be controlled manually with XBox Controller or as part of mission through RVIZ.

To launch Simulation of Submerging and resurfacing:
`roslaunch drone_controller start_submerge.launch`